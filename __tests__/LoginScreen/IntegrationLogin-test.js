import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const email = 'omewa@email.com';
const pass = 'omaewa2022';

const loginApi = async (em, password) => {
  try {
    return await axios.post('http://code.aldipee.com/api/v1/auth/login', {
      em,
      password,
    });
  } catch (e) {
    return [];
  }
};

describe('lOGIN', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  describe('when API call is successful', () => {
    it('Login success', async () => {
      const loginSuccess = {
        user: {
          role: 'user',
          isEmailVerified: true,
          email: 'omewa@email.com',
          name: 'Omewa Chan',
          id: '6242f19b2c4ebe0f1d319442',
        },
      };
      mock
        .onPost('http://code.aldipee.com/api/v1/auth/login')
        .reply(200, loginSuccess);

      const result = await loginApi(email, pass);

      expect(result.data).toEqual(loginSuccess);
    });
  });

  describe('when API call is unsuccessful', () => {
    it('Login failed', async () => {
      mock.onPost('http://code.aldipee.com/api/v1/auth/login').reply(400, []);
      const result = await loginApi(email, pass);
      expect(result).toEqual([]);
    });
  });
});
