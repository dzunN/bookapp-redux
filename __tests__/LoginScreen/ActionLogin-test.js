import ReduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';

import {
  loginAction,
  logout,
  setToken,
  setProfile,
  setLoading,
} from '../../src/store/actions';

jest.mock('axios');

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

test('Login Action Test', () => {
  expect(setProfile('data')).toEqual({
    type: '@SET_PROFILE',
    payload: 'data',
  });
  expect(setToken('token')).toEqual({
    type: '@SET_TOKEN',
    payload: 'token',
  });
  expect(logout()).toEqual({
    type: '@LOGOUT_USER',
  });
});

describe('Login User Test', () => {
  const middlewares = [ReduxThunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore({});

  // Login Success
  test('should Login a  user ', async () => {
    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: {
          token: 'testToken',
          name: 'testName',
        },
      }),
    );

    const testUser = {
      email: 'test@email.com',
      password: 'testPassword',
    };

    await store.dispatch(loginAction(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(
        setLoading(true),
        setToken('testToken'),
      );
    });
  });

  // Login fail
  test('should not Login a  user', async () => {
    axios.mockRejectedValue({
      status: 500,
    });
    const userInfo = {
      name: '',
      email: '',
    };

    await store.dispatch(loginAction(userInfo)).then(() => {
      expect(store.getActions()[0]).toEqual(
        setLoading(true),
        setLoading(false),
      );
    });
  });
});
