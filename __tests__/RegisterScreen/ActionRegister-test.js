import ReduxThunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import {registerAction, setLoading} from '../../src/store/actions';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('axios');

describe('Register User Test', () => {
  const middlewares = [ReduxThunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore({});

  // Register Success
  test('should Register a  user ', async () => {
    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
      }),
    );

    const testUser = {
      name: 'testName',
      email: 'test@email.com',
      password: 'testPassword',
    };

    await store.dispatch(registerAction(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(registerAction(true));
    });
  });

  // Register fail
  test('should not Register a  user', async () => {
    axios.mockRejectedValue({
      status: 500,
    });

    const testUser = {
      name: '',
      email: '',
      password: '',
    };

    await store.dispatch(registerAction(testUser)).then(() => {
      expect(store.getActions()[0]).toEqual(
        setLoading(true),
        setLoading(false),
      );
    });
  });
});
