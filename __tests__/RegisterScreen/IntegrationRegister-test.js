import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const registerApi = async (em, password, name) => {
  try {
    return await axios.post('http://code.aldipee.com/api/v1/auth/register', {
      em,
      password,
      name,
    });
  } catch (e) {
    return [];
  }
};

describe('REGISTER', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });
  describe('registerApi', () => {
    it('should return an object', async () => {
      const response = {
        success: true,
        message:
          'Register succeed! Please try to login with email samawaya@email.com',
        data: {
          role: 'user',
          isEmailVerified: true,
          email: 'samawaya@email.com',
          name: 'Samawa Wanai',
          id: '625d761262fc712fb9a2541b',
        },
      };
      mock
        .onPost('http://code.aldipee.com/api/v1/auth/register')
        .reply(201, response);
      const result = await registerApi(
        'samawaya@email.com',
        'Samawa2022',
        'Samawa Wanai',
      );
      expect(result.data).toEqual(response);
    });
    it('should return an empty object', async () => {
      mock
        .onPost('http://code.aldipee.com/api/v1/auth/register')
        .reply(400, []);
      const result = await registerApi(
        'samawaya@email.com',
        'Samawa2022',
        'Samawa Wanai',
      );
      expect(result).toEqual([]);
    });
  });
});
