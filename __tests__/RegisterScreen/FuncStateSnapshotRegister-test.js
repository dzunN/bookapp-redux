/* eslint-disable jest/valid-expect */
import {shallow, configure} from 'enzyme';
import React from 'react';
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import RegisterScreen from '../../src/screens/Register';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
jest.mock('react-native-share', () => ({}));
jest.mock('@react-navigation/native', () => ({
  useNavigation: component => component,
}));
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

configure({adapter: new Adapter(), disableLifecycleMethods: true});

const mockDispatch = jest.fn();
const registerWrapper = shallow(<RegisterScreen />);

describe('Login Screen test', () => {
  it('should renders `Register Screen` module correctly', () => {
    const tree = renderer.create(<RegisterScreen />);
    expect(tree).toMatchSnapshot();
  });

  describe('Check component', () => {
    it('should create find `Input`', () => {
      expect(registerWrapper.find('Input').exists());
    });

    it('should create `TouchableOpacity` component', () => {
      expect(registerWrapper.find('TouchableOpacity').exists());
    });
    it('should create `LinkComponent` component', () => {
      expect(registerWrapper.find('LinkComponent').exists());
    });
  });
});
