/* eslint-disable prettier/prettier */
/* eslint-disable no-undef */
import { faker } from '@faker-js/faker';

describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  const goLoginPage = async () => {
    await expect(element(by.id('SplashScreen'))).toBeVisible();
    await waitFor(element(by.id('LoginScreen'))).toBeVisible().withTimeout(4000);
    await expect(element(by.id('LoginScreen'))).toBeVisible();
  };

  const doLogin = async (email, password) => {
    await element(by.id('input-email')).typeText(email);
    await element(by.id('input-password')).typeText(password);
    // await element(by.id('input-password')).tapReturnKey();
    await element(by.id('btn-login')).tap();
    await element(by.id('btn-logout')).tap();
  };

  const doRegister = async (FullName,email,password) => {
    await element(by.id('field-FullName')).typeText(FullName);
    await element(by.id('field-email')).typeText(email);
    await element(by.id('field-password')).typeText(password);
    await element(by.id('showHide-password')).tap();
    await element(by.id('showHide-password')).tap();
    await element(by.id('btn-register')).tap();
  };

  const successRegister = async () => {
    await element(by.id('btn-back-to-login')).tap();
  };

  const akun = {
    FullName: 'Ayang Aku',
    email: 'omewa@email.com',
    email2: faker.internet.email(),
    email3: faker.internet.email(),
    password: 'omaewa2022\n',
  };

  describe('Login & Register Scene', () => {
    it('Should do Login', async () => {
      await goLoginPage();
      await doLogin(akun.email, akun.password);
    });

    it('Should do Register', async () => {
      await goLoginPage();
      await element(by.id('btn-go-register')).tap();
      await doRegister(akun.FullName, akun.email2, akun.password);
      await successRegister();
    });

    it('Should do Login after Register', async () => {
      await goLoginPage();
      await element(by.id('btn-go-register')).tap();
      await doRegister(akun.FullName, akun.email3, akun.password);
      await successRegister();
      await doLogin(akun.email3, akun.password);
    });
  });
});
