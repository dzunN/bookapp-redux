import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import Pdf from 'react-native-pdf';
import {Header} from '../../components';
import {COLORS} from '../../themes';

function ReadBook({navigation, route}) {
  const {title, author} = route.params;
  return (
    <ScrollView contentContainerStyle={styles.scroll} testID="register-screen">
      <Header
        title={title}
        subTitle={author}
        onBack={() => navigation.goBack()}
      />
      <View style={styles.page}>
        <Pdf
          source={{
            uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf',
          }}
          style={styles.pdf}
        />
      </View>
    </ScrollView>
  );
}

export default ReadBook;

const styles = StyleSheet.create({
  scroll: {flexGrow: 1, backgroundColor: COLORS.black},
  page: {flex: 1},
  pdf: {
    width: '100%',
    height: '100%',
  },
  pdfWrapper: {
    width: '100%',
    height: '100%',
  },
});
